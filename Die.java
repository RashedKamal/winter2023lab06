import java.util.Random;
public class Die{
    private int faceValue;
    private Random randomGen;

    public Die(){
        this.faceValue = 1;
        Random random = new Random();
        this.randomGen = random;
    }

    public int getFaceValue(){
        return this.faceValue;
    }

    public int roll(){
        this.faceValue = this.randomGen.nextInt(6)+1;
        return this.faceValue;
    }

    @Override
    public String toString(){
        return ("Face value: " + this.faceValue);
    }
}