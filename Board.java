public class Board {
    private Die dice1;
    private Die dice2;
    private boolean[] tiles;

    public Board(){
        this.dice1 = new Die();
        this.dice2 = new Die();
        this.tiles = new boolean[12];
    }

    @Override
    public String toString() {
        String[] tileValues = new String[tiles.length];
        for(int i=0; i<this.tiles.length; i++){
            if(this.tiles[i] == false){
                tileValues[i] = Integer.toString(i+1);
            }
            else{
                tileValues[i] = "X";
            }
        }
        return "Board: {" + tileValues[0] + ", " + tileValues[1] + ", " + tileValues[2] + ", " + tileValues[3] + ", " + tileValues[4] + ", " + tileValues[5] + ", " + tileValues[6] + ", " + tileValues[7] + ", " + tileValues[8] + ", " + tileValues[9] + ", " + tileValues[10] + ", " + tileValues[11] + "}";
    }

    public boolean playATurn(){
        boolean turnOver = false;
        this.dice1 = new Die();
        this.dice2 = new Die();
        dice1.roll();
        dice2.roll();
        System.out.println(dice1.toString() + "(Die 1)");
        System.out.println(dice2.toString() + "(Die 2)");
        int sumOfDice = dice1.getFaceValue()+dice2.getFaceValue();
        if(!this.tiles[sumOfDice-1]){
            this.tiles[sumOfDice-1] = true;
            System.out.println("Closing tile equal to sum: " + sumOfDice);
            turnOver = false;
        }
        else{
            if(!this.tiles[dice1.getFaceValue()-1]){
                this.tiles[dice1.getFaceValue()-1] = true;
                System.out.println("Closing tile with the same value as die one: " + dice1.getFaceValue());
                turnOver = false;
            }
            else if(!this.tiles[dice2.getFaceValue()-1]){
                this.tiles[dice2.getFaceValue()-1] = true;
                System.out.println("Closing tile with the same value as die two: " + dice2.getFaceValue());
                turnOver =  false;
            }
            else{
                System.out.println("All the tiles for these values are already shut");
                turnOver = true;
            }
        }
        return turnOver;
    }
}