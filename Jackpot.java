import java.util.Scanner;
public class Jackpot{
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        int i = 0; // this value will only change if the user wants to quit playing
        int counter = 0; // to count the number of wins
        while(i == 0){
            System.out.println("Welcome to Jackpot!");
            Board board = new Board();
            boolean gameOver = false;
            int numOfTilesClosed = 0;
            while(gameOver != true){
                System.out.println(board.toString());
                if (board.playATurn() == true){
                    gameOver = true;
                }
                else{
                    numOfTilesClosed++;
                }
            }
            System.out.println("Number of tiles closed: " + numOfTilesClosed);
            if(numOfTilesClosed>=7){
                System.out.println("You have reached the jackpot. You win!");
                counter++; // only increases if you win
            }
            else{
                System.out.println("You lost :(");
            }
            System.out.println("Continue? If yes, type 1, if no, type 2");
            int answer = reader.nextInt();
            if(answer == 2){
                i++;
                System.out.println("You have won " + counter + " times!");
            }
        }
    }
}